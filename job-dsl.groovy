def release_branch = "${BRANCH_NAME}"
def repo_name = "${REPOSITORY_NAME}"
def name_first_part = ""
def infra_repo_url = ""
def jenkins_script_path = ""
def split_release_branch = release_branch.split('/')

switch(repo_name) {
    case "shs-bedesigner-services":
        name_first_part = "Build_com.sabre.shs.hss.service.beDesignerService_"
        infra_repo_url = "https://git.prod.sabre.com/scm/shsgcp/java-bedesigner-services.git"
        jenkins_script_path = "shs-bedesigner-services.jenkinsfile"
        break;
    case "shs-infrastructure-services":
        name_first_part = "Build_com.sabre.shs.hss.service.infrastructureService_"
        infra_repo_url = "https://git.prod.sabre.com/scm/shsgcp/java-infrastructure-services.git"
        jenkins_script_path = "shs-infrastructure-services.jenkinsfile"
        break;
    case "shs-orcio-services":
        name_first_part = "Build_com.sabre.shs.hss.service.orcioService_"
        infra_repo_url = "https://git.prod.sabre.com/scm/shsgcp/java-orcio-services.git"
        jenkins_script_path = "shs-orcio-services.jenkinsfile"
        break;
    case "shs-payment-services":
        name_first_part = "Build_com.sabre.shs.hss.service.paymentService_Master_"
        infra_repo_url = "https://git.prod.sabre.com/scm/shsgcp/java-payment-services.git"
        jenkins_script_path = "shs-payment-services.jenkinsfile"
        break;
    case "shs-rules-services":
        name_first_part = "Build_com.sabre.shs.hss.service.rulesService_"
        infra_repo_url = "https://git.prod.sabre.com/scm/shsgcp/java-rule-services.git"
        jenkins_script_path = "shs-rules-services.jenkinsfile"
        break;
    case "shs-security-services":
        name_first_part = "Build_com.sabre.shs.hss.service.securityService_"
        infra_repo_url = "https://git.prod.sabre.com/scm/shsgcp/java-security-services.git"
        jenkins_script_path = "shs-security-services.jenkinsfile"
        break;
}

def new_name = name_first_part + split_release_branch[1]

pipelineJob("${new_name}") {
    definition {
        cpsScm {
            scm {
                git {
                    remote {
                        name('master')
                        url(infra_repo_url)
                        credentials('builduser')
                    }
                }
            }
            scriptPath(jenkins_script_path)
        }
    }
}
