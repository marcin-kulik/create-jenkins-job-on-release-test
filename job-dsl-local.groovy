pipelineJob('job-1') {
    definition {
        cpsScm {
            scm {
                git {
                    remote {
                        name('master')
                        url('https://marcin-kulik@bitbucket.org/marcin-kulik/create-jenkins-job-on-release-test.git')
                    }
                }
            }
            scriptPath('job1/Jenkinsfile')
        }
    }
}
pipelineJob('job-2') {
    definition {
        cpsScm {
            scm {
                git {
                    remote {
                        name('master')
                        url('https://marcin-kulik@bitbucket.org/marcin-kulik/create-jenkins-job-on-release-test.git')
                    }
                }
            }
            scriptPath('job2/Jenkinsfile')
        }
    }
}
pipelineJob('job-3') {
    definition {
        cpsScm {
            scm {
                git {
                    remote {
                        name('master')
                        url('https://marcin-kulik@bitbucket.org/marcin-kulik/create-jenkins-job-on-release-test.git')
                    }
                }
            }
            scriptPath('job3/Jenkinsfile')
        }
    }
}