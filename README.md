Removing Webhooks from a repo and leaving only a Post Webhook changed a trigger payload.

Current payload with one Post Webhook:

![img.png](images/img.png)

look like:

    {
        "actor": {
            "username": "SG0312536",
            "displayName": "Kulik, Marcin",
            "emailAddress": "Marcin.Kulik.ctr@sabre.com"
        },
        "repository": {
            "scmId": "git",
            "project": {
                "key": "~SG0312536",
                "name": "Kulik, Marcin"
            },
            "slug": "shs-bedesigner-services",
            "links": {
                "self": [
                    {
                        "href": "https://git.prod.sabre.com/users/sg0312536/repos/shs-bedesigner-services/browse"
                    }
                ]
            },
            "ownerName": "~SG0312536",
            "public": false,
            "fullName": "~SG0312536/shs-bedesigner-services",
            "owner": {
                "username": "~SG0312536",
                "displayName": "~SG0312536",
                "emailAddress": null
            }
        },
    "push": {
        "changes": [
            {
                "created": true,
                "closed": false,
                "old": null,
                "new": {
                    "type": "branch",
                    "name": "release/1.12",
                    "target": {
                        "type": "commit",
                        "hash": "2189e312f97fb2f41151eb7397aff5f44cfc9863"
                        }
                    }
                }
            ]
        }
    }

and a JSONPath to find the name of the branch is:
    `push.changes[0].new.name`

![img_1.png](images/img_1.png)

Jenkins View PoC - regular expression

    .*(Build_com.sabre.shs.hss.service.beDesignerService.*|Build_com.sabre.shs.hss.service.infrastructureService.*|Build_com.sabre.shs.hss.service.orcioService.*|Build_com.sabre.shs.hss.service.paymentService_Master.*|Build_com.sabre.shs.hss.service.rulesService.*|Build_com.sabre.shs.hss.service.securityService.*).*